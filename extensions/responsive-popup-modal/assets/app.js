// Variable Declarations
var name, top_size, bottom_size, hip_size, sleeve_style=[]
var column = document.querySelector('.column')
var header = document.getElementById('header')
var body = document.getElementById('body')

// Event Listeners
document.addEventListener("DOMContentLoaded", function() {
    setTimeout(()=>{
        document.querySelector('.floating-modal').classList.add('active')
    }, 3000)
});

// Function Definitions
function dismiss() {
    document.querySelector('.floating-modal').classList.remove('active')
}

function askquestions() {
    document.querySelector('.floating-modal').classList.remove('active')
    document.querySelector('.questions-modal').classList.add('active')
}

function closequestions() {
    document.querySelector('.questions-modal').classList.remove('active')
}

function startquiz() {
    getMeasurements()   //Get Measurements from the user (Top Size / Bottom Size / Hip Size)
}

function getMeasurements() {
    document.querySelector('.start-btn').remove()

    name = document.getElementById('name-inp').value
    
    header.innerHTML = `${name}, Help Me With Your Measurements 📏`

    document.getElementById('name-inp').remove()

    body.innerHTML = `Don’t worry if you’re not 100% sure <br/> You can always change this later`

    createLabel('Your Top Size', 'top-lbl')

    createSelect(`<option disabled="">Select tops size</option><option value="XXS/ UK 6/ US 2/ Bust to fit 31 inches">XXS/ UK 6/ US 2/ Bust to fit 31 inches</option><option value="XS/ UK 8/ US 4/ Bust to fit 32 inches">XS/ UK 8/ US 4/ Bust to fit 32 inchess</option><option value="S/ UK 10/ US 6/ Bust to fit 34 inches">S/ UK 10/ US 6/ Bust to fit 34 inches</option><option value="M/ UK 12/ US 8/ Bust to fit 36 inches">M/ UK 12/ US 8/ Bust to fit 36 inches</option><option value="L/ UK 14/ US 10/ Bust to fit 38 inches">L/ UK 14/ US 10/ Bust to fit 38 inches</option><option value="XL/ UK 16/ US 12/ Bust to fit 41 inches">XL/ UK 16/ US 12/ Bust to fit 41 inches</option><option  value="XXL/ UK 18/ US 14/ Bust to fit 43 inches">XXL/ UK 18/ US 14/ Bust to fit 43 inches</option><option value="3XL/ UK 20/ US 16/ Bust to fit 45 inches">3XL/ UK 20/ US 16/ Bust to fit 45 inche</option><option value="4XL/ UK 22/ US 18/ Bust to fit 47 inches">4XL/ UK 22/ US 18/ Bust to fit 47 inches</option><option value="5XL/ UK 24/ US 20/ Bust to fit 49 inches">5XL/ UK 24/ US 20/ Bust to fit 49 inches</option><option value="6XL/ UK 26/ US 22/ Bust to fit 50 inches">6XL/ UK 26/ US 22/ Bust to fit 50 inches</option>`, 'top-size')

    createLabel('Your Bottoms Size', 'bottom-lbl')
    
    createSelect(`<option disabled="">Select bottoms size</option><option value="XXS/ UK 6/ US 2/ Wearing Waist 24 inches">XXS/ UK 6/ US 2/ Wearing Waist 24 inches </option><option value="XS/ UK 8/ US 4/ Wearing Waist 26 inches">XS/ UK 8/ US 4/ Wearing Waist 26 inches</option><option value="S/ UK 10/ US 6/ Wearing Waist 28 inches">S/ UK 10/ US 6/ Wearing Waist 28 inches</option><option value="M/ UK 12/ US 8/ Wearing Waist 30 inches">M/ UK 12/ US 8/ Wearing Waist 30 inches</option><option value="L/ UK 14/ US 10/ Wearing Waist 32 inches">L/ UK 14/ US 10/ Wearing Waist 32 inches </option><option value="XL/ UK 16/ US 12/ Wearing Waist 34 inches">XL/ UK 16/ US 12/ Wearing Waist 34 inches </option><option value="XXL/ UK 18/ US 14/ Wearing Waist 36 inches">XXL/ UK 18/ US 14/ Wearing Waist 36 inches </option><option value="3XL/ UK 20/ US 16/ Wearing Waist 38 inches">3XL/ UK 20/ US 16/ Wearing Waist 38 inches </option><option value="4XL/ UK 22/ US 18/ Wearing Waist 40 inches">4XL/ UK 22/ US 18/ Wearing Waist 40 inches </option><option value="5XL/ UK 24/ US 20/ Wearing Waist 42 inches">5XL/ UK 24/ US 20/ Wearing Waist 42 inches </option>`, 'bottom-size')

    createLabel('Your hips', 'hip-lbl')

    createSelect(`<option disabled="">Select hips size</option><option value="32">32 inches</option><option value="33">33 inches</option><option value="34">34 inches</option><option value="35">35 inches</option><option value="36">36 inches</option><option value="37">37 inches</option><option value="38">38 inches</option><option value="39">39 inches</option><option value="40">40 inches</option><option value="41">41 inches</option><option value="42">42 inches</option><option value="43">43 inches</option><option value="44">44 inches</option><option value="45">45 inches</option><option value="46">46 inches</option><option value="47">47 inches</option><option value="48">48 inches</option><option value="49">49 inches</option><option value="50">50 inches</option><option value="51">51 inches</option><option value="52">52 inches</option><option value="53">53 inches</option><option value="54">54 inches</option>`, 'hip-size')

    createButton('NEXT')

    document.getElementById('NEXT').addEventListener('click', saveMeasurements)
}

function createLabel(name, id){
    var label = document.createElement('label')
    label.innerHTML = name
    label.id = id
    column.appendChild(label)
}

function createSelect(options, id) {
    var select = document.createElement('select')
    select.id=id
    select.innerHTML=options
    column.appendChild(select)
}

function createButton(btn_name) {
    var button = document.createElement('button')
    button.innerHTML=btn_name
    button.classList.add('next-btn')
    button.id = btn_name
    column.appendChild(button)
}

function saveMeasurements() {
    top_size = document.getElementById('top-size').value
    bottom_size = document.getElementById('bottom-size').value
    hip_size = document.getElementById('hip-size').value

    removeMeasurements()
    getSleeveStyle()
}

function removeMeasurements() {
    removeElement('top-lbl')
    removeElement('bottom-lbl')
    removeElement('hip-lbl')
    removeElement('top-size')
    removeElement('bottom-size')
    removeElement('hip-size')
    removeElement('NEXT')
}

function removeElement(id) {
    document.getElementById(id).remove()
}

function getCheckBoxValue(e) {
    if (e.target.checked) {
        sleeve_style.push(e.target.id)
    }else {
        sleeve_style.splice(sleeve_style.indexOf(e.target.id), 1)
    }
}

function createCheckBox(id) {
    var div = document.createElement('div')
    div.id = id
    div.className = 'checkbox-container'
    var checkBox = document.createElement('input')
    checkBox.className = 'checkBox'
    checkBox.type='checkbox'
    checkBox.id = id
    checkBox.name = id

    checkBox.addEventListener('click', getCheckBoxValue)

    var label = document.createElement('label')
    label.innerHTML = id
    label.htmlFor = id
    
    div.appendChild(checkBox)
    div.appendChild(label)
    
    column.appendChild(div)
}

function getSleeveStyle() {
    header.innerHTML = `Sleeve styles that you wear`
    body.innerHTML = `You can select multiple options`
    
    createCheckBox('Sleeveless')
    createCheckBox('Cap Sleeves')
    createCheckBox('Half Sleeves')
    createCheckBox('3/4th Sleeves')
    createCheckBox('Full Sleeves')

    createButton('NEXT')
    document.getElementById('NEXT').addEventListener('click', showResults)
}

function removeSleeveStyle() {
    removeElement('Sleeveless')
    removeElement('Cap Sleeves')
    removeElement('Half Sleeves')
    removeElement('3/4th Sleeves')
    removeElement('Full Sleeves')
    removeElement('NEXT')
}

function showResults() {

    removeSleeveStyle()

    header.innerHTML = `Results`
    body.innerHTML = `Here are styles recommended for you based on your preferences...`

    var table = document.createElement('table')

    table.innerHTML = `
    <tr>
      <th>Your Top Size : </th>
      <td>${top_size}</td>
    </tr>
    <tr>
      <th>Your Bottom Size : </th>
      <td>${bottom_size}</td>
    </tr>
    <tr>
      <th>Your Hip Size : </th>
      <td>${hip_size}</td>
    </tr>
    <tr>
      <th>Sleeve Style Preferences : </th>
      <td>${sleeve_style}</td>
    </tr>
    `
    column.appendChild(table)

    createButton('DONE')
    document.getElementById('DONE').addEventListener('click', ()=> {
        window.location.href = `https://stylenook-blackcurrant.myshopify.com/collections/all?filter.v.price.gte=&filter.v.price.lte=&filter.p.product_type=Kurtas&sort_by=title-ascending`
    })
}

